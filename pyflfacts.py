# This tool is for exporting data from the Florida Accountability Tracking
# System (FACTS) for usage in a Postgres database.
#
# The recommended means of downloading data is via the CSV export made available
# after searching for a particular date range for contracts at:
#
#  https://facts.fldfs.com/Search/ContractAdvancedSearch.aspx
#
# Note that FACTS does not properly escape strings containing quotes in its
# CSV exports. For example, it has exported the CSV row:
#
#  "AGENCY FOR PERSONS WITH DISABILITIES","FIVE "S", INC.                 ","Purchase Order","","B55A6D","","SAPP'S SAW & MOWER CENTER      ","","2400.00","Multiple","Multiple","","","2400.00","BLKT-6732-FY19/20-AHINES-MMARTIN-MAINTENANCE/GROUNDS-SAPPS SAW & MOWER CENTER","Receiving","","","7/1/2019","6/30/2020","","","","7/1/2019","","","Purchase under $2,500 {Rule 60A-1.002(2), FAC]","","","","","","","","","","","","","","","","","","","","","","","","",""
#
# where the string
#
#  FIVE "S", INC.
#
# did not have its double-quotes escaped. The easiest solution is to manually
# escape said double-quotes through conveting them into double-quotes, i.e.,
#
#  FIVE ""S"", INC.
#
# Given a line number in Pandas where a problem occurred, e.g.,
#
#  pandas.errors.ParserError: Error tokenizing data. C error: Expected 52 fields in line 9834, saw 53
#
# one can add two to the reported line number to know roughly which row
# (zero-indexed) to manually correct in the original CSV (i.e., 9836).
#
# But we attempt to automatically correct such errors.
#
import csv
import json
import numpy as np
import pandas as pd
import re

FACTS_FLOATS = [
    'Original Contract Amount', 'Total Amount', 'PO Budget Amount',
    'Value of Capital Improvements'
]

FACTS_DATETIMES = [
    'Begin Date', 'Original End Date', 'New End Date',
    'Contract Execution Date', 'Grant Award Date', 'PO Order Date',
    'Business Case Date'
]

# We append any 'Vendor/Grantor Name Line 2' information to
# 'Vendor/Grantor Name'.
FACTS_SIMPLIFIED_NAMES = {
    'Agency Name': 'agency',
    'Vendor/Grantor Name': 'vendor',
    'Type': 'type',
    'Agency Contract ID': 'agency_contract_id',
    'PO Number': 'po_number',
    'Grant Award ID': 'grant_award_id',
    'Original Contract Amount': 'orig_contract_amount',
    'Total Amount': 'total_amount',
    'Recurring Budgetary Amount': 'recurring_budgetary_amount',
    'Non Recurring Budgetary Amount': 'non_recurring_budgetary_amount',
    'PO Budget Amount': 'po_budget_amount',
    'Commodity/Service Type Code': 'commodity_type_code',
    'Commodity/Service Type Description': 'commodity_type_description',
    'Short Title': 'short_title',
    'Long Title/PO Title': 'long_title',
    'Status': 'status',
    'FLAIR Contract ID': 'flair_contract_id',
    'Begin Date': 'begin_date',
    'Original End Date': 'original_end_date',
    'New End Date': 'new_end_date',
    'Contract Execution Date': 'contract_execution_date',
    'Grant Award Date': 'grant_award_date',
    'PO Order Date': 'po_order_date',
    'Agency Service Area': 'agency_service_area',
    'Authorized Advanced Payment': 'authorized_advanced_payment',
    'Method of Procurement': 'method_of_procurement',
    'State Term Contract ID': 'state_term_contract_id',
    'Agency Reference Number': 'agency_reference_number',
    'Contract Exemption Explanation': 'contract_exemption_explanation',
    'Statutory Authority': 'statutory_authority',
    'Recipient Type': 'recipient_type',
    'Contract Involves State or Federal Aid': 'involves_gov_aid',
    'Provide Administrative Cost': 'provide_admin_cost',
    'Administrative Cost Percentage': 'admin_cost_percent',
    'Provide for Periodic Increase': 'provide_periodic_increase',
    'Periodic Increase Percentage': 'periodic_increase_percent',
    'Business Case Study Done': 'business_case_study',
    'Business Case Date': 'business_case_date',
    'Legal Challenges to Procurement': 'legal_challenges',
    'Legal Challenge Description': 'legal_challenge_description',
    'Was the Contracted Functions Previously Done by the State':
    'previously_done_by_state',
    'Was the Contracted Functions Considered for Insourcing back to the State':
    'considered_for_insourcing',
    'Did the Vendor Make Capital Improvements on State Property':
    'improvement_on_state_property',
    'Capital Improvement Description': 'improvement_description',
    'Value of Capital Improvements': 'improvement_value',
    'Value of Unamortized Capital Improvements':
    'improvement_unamortized_value',
    'Comment': 'comment',
    'CFDA Code': 'cfda_code',
    'CFDA Description': 'cfda_description',
    'CSFA Code': 'csfa_code',
    'CSFA Description': 'csfa_description'
}


def csv_to_df(csv_name, corrected_csv_name='corrected.csv'):
    with open(csv_name) as infile:
        lines = infile.readlines()
        output_lines = [lines[0]]
        for line in lines[1:]:
            # Attempt to correct unescaped quotes in the middle of a string.
            output_line = re.sub(r'([^,"])"(?!(["\n]|,"))', r'\1""', line)

            # Attempt to correct unescaped quotes at the beginning of a string.
            output_line = re.sub(r'",""(?!(["\n]|,"))', r'","""', output_line)

            output_lines.append(output_line)
        with open(corrected_csv_name, 'w') as outfile:
            outfile.writelines(output_lines)

    df = pd.read_csv(corrected_csv_name, dtype=object)

    # Strip the strings.
    df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    # Join the vendor second line onto the first, then drop the second.
    VENDOR_COLUMNS = ['Vendor/Grantor Name', 'Vendor/Grantor Name Line 2']
    df[VENDOR_COLUMNS] = df[VENDOR_COLUMNS].astype(str)
    df['Vendor/Grantor Name'] = df[VENDOR_COLUMNS].agg(' '.join, axis=1)
    df = df.drop(columns=['Vendor/Grantor Name Line 2'])

    # Strip the strings.
    df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    # Coerce the numeric types out of strings.
    for float_column in FACTS_FLOATS:
        df[float_column] = pd.to_numeric(df[float_column], errors='coerce')
        df[float_column] = df[float_column].astype(np.float32)

    # Force date columns to datetime
    for datetime_column in FACTS_DATETIMES:
        df[datetime_column] = pd.to_datetime(df[datetime_column],
                                             format='%m/%d/%Y',
                                             errors='coerce')

    df = df.rename(columns=FACTS_SIMPLIFIED_NAMES)

    # Ensure the whitespace is merged in the vendor column.
    df.replace({'vendor': r'\s+'}, value=' ', inplace=True, regex=True)

    return df


def write_unique_vendors(df, unique_vendors_json):
    unique_names = sorted([x.lower() for x in df['vendor'].unique()])
    with open(unique_vendors_json, 'w') as outfile:
        json.dump(unique_names, outfile, indent=1)


def export_df_to_csv(df, export_csv_name):
    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    df = df.replace(r'\\+;', r';', regex=True)

    df.drop_duplicates(inplace=True, ignore_index=True)

    df.to_csv(export_csv_name,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)


def process_csv(orig_csv_name,
                output_csv='us_fl_facts_filing.csv',
                corrected_csv_name='corrected.csv'):
    df = csv_to_df(orig_csv_name, corrected_csv_name=corrected_csv_name)
    export_df_to_csv(df, output_csv)
